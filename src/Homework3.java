import java.math.BigInteger;
import java.util.Random;

/**
 * Emelie Gage, CSIS 180 Homework 3
 * RSA(m) = m^e(mod n)
 * RSA^-1(r) = r^d(mod n)
 * 1) publish pair P = (e,n) public key
 * 2) hide S = (d,n) secret key
 */
public class Homework3 {
    public static void main(String[] args) {
        Random rand = new Random();
        BigInteger p, q, e, m, d, phi, n, r, m2;
        m = new BigInteger("123456789");
        p = BigInteger.probablePrime(100, rand);
        q = BigInteger.probablePrime(100, rand);
        n = p.multiply(q);
        phi = ((p.subtract(BigInteger.ONE))).multiply((q.subtract(BigInteger.ONE)));
        e = BigInteger.probablePrime(8, rand);
        if((phi.divide(e)).equals(BigInteger.ONE)){
            e = BigInteger.probablePrime(8, rand);
        }
        d = e.modInverse(phi);
        r = m.modPow(e, n);
        m2 = r.modPow(d, n);
        System.out.println("D: "+d+"\n"+"E: "+e+"\n"+"P: "+p+"\n"+"Q: "+q+"\n"+"N: "+n+"\n"
                +"R:"+r+"\n"+"M: "+m+"\n"+"M2: "+m2);
    }
}
